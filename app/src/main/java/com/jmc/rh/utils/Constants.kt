package com.jmc.rh.utils

class Constants {
    companion object{

        // Constants for the employee attributes
        const val EMAIL = "email"
        const val ID = "id"
        const val PHONE = "phone"
        const val POSITION = "position"
        const val SALARY = "salary"
        const val UPPER_RELATION = "upperRelation"

        const val EMPLOYEE = "employee"

        // Constants for define status of employee
        const val NEW_EMPLOYEE = 1
        const val OLD_EMPLOYEE = 0

    }
}