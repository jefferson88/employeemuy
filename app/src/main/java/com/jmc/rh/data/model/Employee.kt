package com.jmc.rh.data.model

import android.os.Parcelable
import androidx.room.Entity
import androidx.room.PrimaryKey
import kotlinx.android.parcel.Parcelize

@Parcelize
@Entity
data class Employee(val name: String,
                    val email: String,
                    @PrimaryKey val id: Int,
                    val phone: String,
                    val position: String,
                    val salary: String,
                    val upperRelation: Int,
                    val isNew: Int): Parcelable