package com.jmc.rh.data.repository.local.dao

import androidx.lifecycle.LiveData
import androidx.room.*
import com.jmc.rh.data.model.Employee

@Dao
interface EmployeeDao{

    @Query("SELECT * FROM Employee ORDER BY isNew, upperRelation")
    fun getAll(): LiveData<List<Employee>>

    @Query("SELECT * FROM Employee WHERE upperRelation > :relation")
    fun getAllSubsPosition(relation: Int): LiveData<List<Employee>>

    @Insert(onConflict = OnConflictStrategy.IGNORE)
    fun inserAll( list: List<Employee>)

    @Query("UPDATE Employee SET isNew = :isNew")
    fun updateStatusEmployee(isNew: Int)

}