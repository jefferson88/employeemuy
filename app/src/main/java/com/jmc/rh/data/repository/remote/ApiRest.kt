package com.jmc.rh.data.repository.remote

import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory

class ApiRest {

    companion object{
        fun create(): ApiRestMethods{
            val retrofit = Retrofit.Builder()
                .baseUrl(ApiURL.URL_BASE)
                .addConverterFactory(GsonConverterFactory.create())
                .build()

            return retrofit.create(ApiRestMethods::class.java)
        }
    }
}