package com.jmc.rh.data.repository.local

import androidx.lifecycle.LiveData
import com.jmc.rh.data.model.Employee

class EmployeeRepository {

    fun insert(employees: List<Employee>) {
        AppDatabase.getInstance()?.employeeDao()?.inserAll(employees)
    }

    fun getAll(): LiveData<List<Employee>>? = AppDatabase.getInstance()?.employeeDao()?.getAll()

    fun getAllSubsPosition(relation: Int): LiveData<List<Employee>>? = AppDatabase.getInstance()?.employeeDao()?.getAllSubsPosition(relation)

    fun updateStatusEmployee(isNew: Int){
        AppDatabase.getInstance()?.employeeDao()?.updateStatusEmployee(isNew)
    }

}