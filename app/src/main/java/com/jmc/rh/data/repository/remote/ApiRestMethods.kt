package com.jmc.rh.data.repository.remote

import com.google.gson.JsonObject
import retrofit2.Call
import retrofit2.http.GET

interface ApiRestMethods {

    @GET(ApiURL.URL_LIST)
    fun getEmployees(): Call<JsonObject>

}