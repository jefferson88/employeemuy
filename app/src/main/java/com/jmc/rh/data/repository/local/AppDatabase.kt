package com.jmc.rh.data.repository.local

import androidx.room.Database
import androidx.room.Room
import androidx.room.RoomDatabase
import com.jmc.rh.MyApp
import com.jmc.rh.data.model.Employee
import com.jmc.rh.data.repository.local.dao.EmployeeDao

@Database(entities = arrayOf(Employee::class), version = 2, exportSchema = false)
abstract class AppDatabase: RoomDatabase() {
    abstract fun employeeDao(): EmployeeDao

    companion object {
        private var INSTANCE: AppDatabase? = null

        fun getInstance(): AppDatabase? {

            INSTANCE ?: synchronized(this) {
                INSTANCE = Room.databaseBuilder(
                    MyApp.getContext().applicationContext,
                    AppDatabase::class.java,
                    "database"
                )
                    .fallbackToDestructiveMigration()
                    .build()
            }

            return INSTANCE
        }

    }


}