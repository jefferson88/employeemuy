package com.jmc.rh.ui.list.adapter

import android.view.View
import androidx.recyclerview.widget.RecyclerView
import kotlinx.android.synthetic.main.card_employee.view.*

class EmployeeViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
    val txtName = itemView.txt_list_name
    val txtPosition = itemView.txt_list_position
    val txtNew = itemView.txt_list_new
    val llList = itemView.ll_list
}