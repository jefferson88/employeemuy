package com.jmc.rh.ui.list

import android.widget.Toast
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.google.gson.JsonObject
import com.jmc.rh.MyApp
import com.jmc.rh.R
import com.jmc.rh.data.model.Employee
import com.jmc.rh.data.repository.local.EmployeeRepository

import com.jmc.rh.data.repository.remote.ApiRest
import com.jmc.rh.utils.Constants
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class ListViewModel : ViewModel(){
    private lateinit var repository: EmployeeRepository

    private val employees: ArrayList<Employee> = ArrayList()

    init {
        repository = EmployeeRepository()
        loadDataRemote()
    }

    /**
     * get data of employees from source remote
     */
    fun loadDataRemote() {

        ApiRest.create()
            .getEmployees().enqueue(object : Callback<JsonObject> {
                override fun onFailure(call: Call<JsonObject>, t: Throwable) {
                    Toast.makeText(MyApp.getContext(), R.string.failed_getdata, Toast.LENGTH_SHORT).show()
                }

                override fun onResponse(call: Call<JsonObject>, response: Response<JsonObject>) {
                    if(response.isSuccessful) {
                        val employeesBody = response.body()
                        val members = employeesBody?.keySet()

                        members?.forEach {
                            val employeeJson = employeesBody.getAsJsonObject(it)

                            val employee = Employee(
                                it,
                                employeeJson.get(Constants.EMAIL).asString,
                                employeeJson.get(Constants.ID).asInt,
                                employeeJson.get(Constants.PHONE).asString,
                                employeeJson.get(Constants.POSITION).asString,
                                employeeJson.get(Constants.SALARY).asString,
                                employeeJson.get(Constants.UPPER_RELATION).asInt,
                                Constants.NEW_EMPLOYEE
                            )

                            employees.add(employee)
                        }

                        viewModelScope.launch(Dispatchers.IO) {
                            repository.updateStatusEmployee(Constants.OLD_EMPLOYEE)
                            repository.insert(employees)
                        }

                    } else {
                        Toast.makeText(MyApp.getContext(), R.string.failed_getdata, Toast.LENGTH_SHORT).show()
                    }
                }

            })
    }


    /**
     * get list of employee from database
     */
    fun getDataFromDB(): LiveData<List<Employee>>? = repository.getAll()

}