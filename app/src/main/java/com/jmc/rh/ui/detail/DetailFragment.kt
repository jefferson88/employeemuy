package com.jmc.rh.ui.detail


import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.animation.AnimationUtils
import android.widget.TextView
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.jmc.rh.MyApp

import com.jmc.rh.R
import com.jmc.rh.data.model.Employee
import com.jmc.rh.ui.list.adapter.EmployeeAdapter
import com.jmc.rh.utils.Constants
import kotlinx.android.synthetic.main.fragment_detail.view.*

/**
 * A simple [Fragment] subclass.
 */
class DetailFragment : Fragment() {

    private lateinit var viewModel: DetailViewModel
    private lateinit var rvSub: RecyclerView
    private lateinit var txtTitle: TextView

    private val animation = AnimationUtils.loadAnimation(MyApp.getContext(), R.anim.right_in)

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        val view = inflater.inflate(R.layout.fragment_detail, container, false)

        setupView(view)
        setupViewModels()

        arguments?.let {
            val employee = it.getParcelable<Employee>(Constants.EMPLOYEE)
            setupData(view, employee)
            loadData(employee!!.upperRelation)
        }

        return view
    }

    /**
     * setup all viewModels
     */
    fun setupViewModels() {
        viewModel = ViewModelProviders.of(this)
            .get(DetailViewModel::class.java)
    }

    /**
     * setup data of employee
     */
    fun setupData(view: View, employee: Employee?) {
        view.txt_detail_name.text = employee?.name
        view.txt_detail_email.text = getString(R.string.email).plus(" ").plus( employee?.email)
        view.txt_detail_phone.text = getString(R.string.phone).plus(" ").plus( employee?.phone)
        view.txt_detail_position.text = getString(R.string.position).plus(" ").plus( employee?.position)
        view.txt_detail_salary.text = getString(R.string.salary).plus(" $").plus( employee?.salary)
        view.txt_detail_new.text = if(employee?.isNew == Constants.NEW_EMPLOYEE) MyApp.getContext().getString(R.string.new_employee) else MyApp.getContext().getString(R.string.old_employee)
    }

    /**
     * setup the view components
     */
    fun setupView(view: View) {
        rvSub = view.rv_detail_subposition
        txtTitle = view.txt_detail_title

        rvSub.layoutManager = LinearLayoutManager(context)

        view.ll_detail_data.startAnimation(animation)
    }

    /**
     * load all data of employee
     */
    fun loadData(relation: Int) {
        viewModel.getSubPosition(relation).observe(this, Observer {
            txtTitle.visibility = if(it.isEmpty()) View.GONE else View.VISIBLE
            rvSub.adapter = EmployeeAdapter(it) { _, _ -> }
        })
    }


}
