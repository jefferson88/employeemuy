package com.jmc.rh.ui.list.adapter

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.animation.AnimationUtils
import androidx.recyclerview.widget.RecyclerView
import com.jmc.rh.MyApp
import com.jmc.rh.R
import com.jmc.rh.data.model.Employee
import com.jmc.rh.utils.Constants

class EmployeeAdapter(val list: List<Employee>, val click: (View, Employee) -> Unit) : RecyclerView.Adapter<EmployeeViewHolder>() {

    private val animation = AnimationUtils.loadAnimation(MyApp.getContext(), R.anim.left_in)

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): EmployeeViewHolder {
        val view = LayoutInflater.from(parent.context).inflate(R.layout.card_employee, parent, false)
        return EmployeeViewHolder(view)
    }

    override fun getItemCount(): Int = list.size

    override fun onBindViewHolder(holder: EmployeeViewHolder, position: Int) {
        val data = list[position]
        holder.txtName.text = data.name
        holder.txtPosition.text = data.position
        holder.txtNew.text = if(data.isNew == Constants.NEW_EMPLOYEE) MyApp.getContext().getString(R.string.new_employee) else MyApp.getContext().getString(R.string.old_employee)
        holder.llList.setOnClickListener{
            click(it, data)
        }

        holder.llList.startAnimation(animation)
    }

}