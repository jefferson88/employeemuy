package com.jmc.rh.ui.detail

import androidx.lifecycle.LiveData
import androidx.lifecycle.ViewModel
import com.jmc.rh.data.model.Employee
import com.jmc.rh.data.repository.local.EmployeeRepository

class DetailViewModel : ViewModel() {

    private lateinit var repository: EmployeeRepository

    init {
        repository = EmployeeRepository()
    }

    /**
     * get data of position from database
     */
    fun getSubPosition(relation: Int) : LiveData<List<Employee>> {
        return repository.getAllSubsPosition(relation)!!
    }
}