package com.jmc.rh.ui.list


import android.os.Bundle
import android.text.Editable
import android.text.TextWatcher
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.EditText
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import androidx.navigation.Navigation
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView

import com.jmc.rh.R
import com.jmc.rh.data.model.Employee
import com.jmc.rh.ui.list.adapter.EmployeeAdapter
import com.jmc.rh.utils.Constants
import kotlinx.android.synthetic.main.fragment_list.view.*

/**
 * A simple [Fragment] subclass.
 */
class ListFragment : Fragment() {

    private lateinit var viewModel: ListViewModel
    private lateinit var rvList: RecyclerView
    private lateinit var edtNane: EditText
    private var list: List<Employee>? = null

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        val view = inflater.inflate(R.layout.fragment_list, container, false)

        setupViewMoldes()
        setupView(view)
        loadData()
        editeTextListener()

        return view
    }

    /**
     * setupa all viewmodels
     */
    fun setupViewMoldes() {
        viewModel = ViewModelProviders.of(this)
            .get(ListViewModel::class.java)
    }

    /**
     * setupa all components of the view
     */
    fun setupView(view: View) {
        rvList = view.rv_list
        edtNane = view.ed_list_name

        rvList.layoutManager = LinearLayoutManager(context)
    }

    /**
     * get all data of viewmodel
     */
    fun loadData() {
        viewModel.getDataFromDB()?.observe(this, Observer {
            list = it
            reloadList("")
        })
    }

    /**
     * listener for editText for filter employee
     */
    fun editeTextListener() {
        edtNane.addTextChangedListener(object : TextWatcher {

            override fun afterTextChanged(s: Editable) {}

            override fun beforeTextChanged(s: CharSequence, start: Int, count: Int, after: Int) {}

            override fun onTextChanged(s: CharSequence, start: Int, before: Int, count: Int) {
                reloadList(s.toString())
            }
        })
    }

    /**
     * reload adapter with the list of employees
     */
    fun reloadList(name: String) {
        val res = list?.filter { it.name.toUpperCase().trim().contains(name.toUpperCase().trim()) }
        res?.let {
            rvList.adapter = EmployeeAdapter(it) { view, employee ->
                val bundle = Bundle()
                bundle.putParcelable(Constants.EMPLOYEE, employee)
                Navigation.findNavController(view).navigate( R.id.to_detail, bundle)
            }
        }
    }

}
